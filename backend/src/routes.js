import { Router } from 'express';
import multer from 'multer';

import uploadConfig from './config/upload';

import ContractController from './controllers/ContractController';
import PartController from './controllers/PartController';

const routes = new Router();
const upload = multer(uploadConfig);

routes.get('/contracts', ContractController.getAll);
routes.get('/contracts/:id', ContractController.get);
routes.post('/contracts', upload.single('file'), ContractController.create);
routes.put('/contracts/:id', upload.single('file'), ContractController.update);
routes.delete('/contracts/:id', ContractController.delete);

routes.get('/parts', PartController.getAll);
routes.get('/parts/:id', PartController.get);
routes.post('/parts', PartController.create);
routes.put('/parts/:id', PartController.update);
routes.delete('/parts/:id', PartController.delete);

export default routes;
