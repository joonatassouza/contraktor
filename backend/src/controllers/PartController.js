import contracts from '../data/Contracts';
import parts from '../data/Parts';
import { max } from '../utils/modelUtils';

export default {
  getAll(request, response) {
    const contractId = parseInt(request.query.contractId, 0);

    const contractIndex = contracts.findIndex(item => item.id === contractId);

    if (contractIndex === -1) {
      return response.status(400).json({ message: 'Contract not found' });
    }

    return response.json(parts.filter(item => item.contractId === contractId));
  },

  get(request, response) {
    const id = parseInt(request.params.id, 0);

    const partIndex = parts.findIndex(item => item.id === id);

    if (partIndex === -1) {
      return response.status(400).json({ message: 'Part not found' });
    }

    return response.json(parts[partIndex]);
  },

  create(request, response) {
    const { firstname, lastname, email, cpf, phone, contractId } = request.body;

    const nextId = max(parts, 'id') + 1;

    const part = {
      id: nextId,
      firstname,
      lastname,
      email,
      cpf,
      phone,
      contractId,
    };

    parts.push(part);

    return response.json(part);
  },

  update(request, response) {
    const id = parseInt(request.params.id, 0);
    const { firstname, lastname, email, cpf, phone, contractId } = request.body;

    const partIndex = parts.findIndex(item => item.id === id);

    if (partIndex === -1) {
      return response.status(400).json({ message: 'Part not found' });
    }

    parts[partIndex] = {
      id,
      firstname: firstname || parts[partIndex].firstname,
      lastname: lastname || parts[partIndex].lastname,
      email: email || parts[partIndex].email,
      cpf: cpf || parts[partIndex].cpf,
      phone: phone || parts[partIndex].phone,
      contractId: contractId || parts[partIndex].cpcontractIdf,
    };

    return response.json(parts[partIndex]);
  },

  delete(request, response) {
    const id = parseInt(request.params.id, 0);

    const partIndex = parts.findIndex(item => item.id === id);

    if (partIndex === -1) {
      return response.status(400).json({ message: 'Part not found' });
    }

    parts.splice(partIndex, 1);

    return response.json({ ok: true });
  },
};
