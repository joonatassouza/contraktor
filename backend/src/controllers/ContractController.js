import contracts from '../data/Contracts';
import parts from '../data/Parts';
import { max } from '../utils/modelUtils';

export default {
  getAll(request, response) {
    return response.json(contracts);
  },

  get(request, response) {
    const id = parseInt(request.params.id, 0);

    const contractIndex = contracts.findIndex(item => item.id === id);

    if (contractIndex === -1) {
      return response.status(400).json({ message: 'Contract not found' });
    }

    const contract = {
      ...contracts[contractIndex],
      parts: parts.filter(item => item.contractId === id),
    };

    return response.json(contract);
  },

  create(request, response) {
    const { title, startDate, endDate } = request.body;
    const { filename: file } = request.file;

    const nextId = max(contracts, 'id') + 1;

    const contract = {
      id: nextId,
      title,
      startDate,
      endDate,
      file,
    };

    contracts.push(contract);

    return response.json(contract);
  },

  update(request, response) {
    const id = parseInt(request.params.id, 0);
    const { title, startDate, endDate } = request.body;

    const contractIndex = contracts.findIndex(item => item.id === id);

    if (contractIndex === -1) {
      return response.status(400).json({ message: 'Contract not found' });
    }

    let { file } = contracts[contractIndex];

    if (request.file) {
      file = request.file.filename;
    }

    contracts[contractIndex] = {
      id,
      title: title || contracts[contractIndex].title,
      startDate,
      endDate,
      file,
    };

    return response.json(contracts[contractIndex]);
  },

  delete(request, response) {
    const id = parseInt(request.params.id, 0);

    const contractIndex = contracts.findIndex(item => item.id === id);

    if (contractIndex === -1) {
      return response.status(400).json({ message: 'Contract not found' });
    }

    contracts.splice(contractIndex, 1);

    return response.json({ ok: true });
  },
};
