export function max(array, property) {
  return array.reduce((maxItem, item) => Math.max(item[property], maxItem), 0);
}
