export default [
  {
    id: 1,
    firstname: 'Jonatas',
    lastname: 'Souza',
    email: 'jonatasfelipe2@hotmail.com',
    cpf: '00011122233',
    phone: '45999999999',
    contractId: 1,
  },
  {
    id: 2,
    firstname: 'Merabe',
    lastname: 'Machado',
    email: 'merabetuani@hotmail.com',
    cpf: '44455566677',
    phone: '45999999999',
    contractId: 1,
  },
];
