import multer from 'multer';
import path from 'path';

export default {
  // eslint-disable-next-line new-cap
  storage: new multer.diskStorage({
    destination: path.resolve(__dirname, '..', '..', 'uploads'),
    filename(request, file, callback) {
      callback(null, file.originalname.split(' ').join(''));
    },
  }),
};
