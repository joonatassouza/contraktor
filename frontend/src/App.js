import React from 'react';
import { registerLocale, setDefaultLocale } from 'react-datepicker';
import pt from 'date-fns/locale/pt-BR';

import Routes from './routes';

import GlobalStyle from './styles/global';
import 'react-datepicker/dist/react-datepicker.css';

function App() {
  setDefaultLocale('es');
  registerLocale('pt_BR', pt);

  return (
    <>
      <Routes />
      <GlobalStyle />
    </>
  );
}

export default App;
