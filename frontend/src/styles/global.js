import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap');

  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  *:focus {
    outline: 0;
  }

  html, body, #root {
    height: 100%;
    background: linear-gradient(-270deg, #dbdbdb, #f1f1f1);
  }

  body {
    -webkit-font-smoothing: antialiased;
  }

  body, input, button {
    font: 14px 'Roboto', sans-serif;
  }

  a {
    text-decoration: none
  }

  ul {
    list-style: none;
  }

  button {
    cursor: pointer;
  }

  input {
    width: 400px;
    height: 30px;
    background: rgba(0, 0, 0, 0.3);
    color: #fff;
    border: none;
    border-radius: 5px;
    margin: 5px;
    padding: 5px 10px;
  }
`;
