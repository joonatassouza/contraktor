import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  height: 100%;
  justify-content: center;
  align-items: center;
`;

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  height: 200px;
  width: 450px;
`;

export const Box = styled.div`
  display: flex;
  justify-content: space-between;
  height: 100%;
  width: 200px;

  a {
    display: flex;
    flex: 1;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    text-align: center;

    background: #2d2d2d;
    color: #fff;
    font-size: 18px;
    font-weight: bold;

    border: none;
    border-radius: 10%;
    box-shadow: 5px 5px 5px #888888;

    &:hover {
      background: #fff;
      color: #2d2d2d;

      svg {
        width: 60px;
        height: 60px;
      }
    }

    transition: 300ms all ease;

    svg {
      margin-bottom: 10px;
      transition: 300ms all ease;
    }
  }
`;
