import React from 'react';
import { Link } from 'react-router-dom';
import { FaFileAlt } from 'react-icons/fa';
import { MdFindInPage } from 'react-icons/md';

import { Container, Box, Wrapper } from './styles';

export default function Main() {
  return (
    <Wrapper>
      <Container>
        <Box>
          <Link to="/newcontract/0">
            <FaFileAlt size={40} />
            Novo Contrato
          </Link>
        </Box>
        <Box>
          <Link to="/contract">
            <MdFindInPage size={40} />
            Visualizar Contratos
          </Link>
        </Box>
      </Container>
    </Wrapper>
  );
}
