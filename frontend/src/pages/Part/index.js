import React, { useState, useEffect } from 'react';
import * as Yup from 'yup';

import api from '../../services/api';

import { Form, Title, Button } from './styles';

const schema = Yup.object().shape({
  firstname: Yup.string().required('Obrigatório'),
  lastname: Yup.string().required('Obrigatório'),
  email: Yup.string()
    .email()
    .required('Obrigatório'),
  cpf: Yup.string()
    .min(11, 'É necessário ter 11 caracteres')
    .max(11, 'É necessário ter 11 caracteres')
    .required('Obrigatório'),
});

export default function Part({ contractId, onClose, id }) {
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [email, setEmail] = useState('');
  const [cpf, setCpf] = useState('');
  const [phone, setPhone] = useState('');
  const [validation, setValidation] = useState({});

  useEffect(() => {
    async function loadPart() {
      const response = await api.get(`parts/${id}`);

      setFirstname(response.data.firstname);
      setLastname(response.data.lastname);
      setEmail(response.data.email);
      setCpf(response.data.cpf);
      setPhone(response.data.phone);
    }

    if (id) loadPart();
  }, [id]);

  async function handleSubmit(e) {
    e.preventDefault();

    try {
      const data = {
        firstname,
        lastname,
        email,
        cpf,
        phone,
        contractId: Number(contractId),
      };

      await schema.validate(data, { abortEarly: false });

      if (id) {
        await api.put(`parts/${id}`, data);
      } else {
        await api.post('parts', data);
      }

      onClose();
    } catch (err) {
      const validations = {};
      if (err.inner) {
        for (const error of err.inner) {
          validations[error.path] = error.message;
        }
      }

      setValidation(validations);
    }
  }

  return (
    <>
      <Title>Nova parte</Title>
      <Form onSubmit={handleSubmit}>
        <input
          type="text"
          name="firstname"
          placeholder="Primeiro nome"
          value={firstname}
          onChange={({ target }) => {
            validation.firstname = '';
            setFirstname(target.value);
          }}
        />
        {validation.firstname && <span>{validation.firstname}</span>}
        <input
          type="text"
          name="secondname"
          placeholder="Sobrenome"
          value={lastname}
          onChange={({ target }) => {
            validation.lastname = '';
            setLastname(target.value);
          }}
        />
        {validation.lastname && <span>{validation.lastname}</span>}
        <input
          type="email"
          name="email"
          placeholder="E-mail"
          value={email}
          onChange={({ target }) => {
            validation.email = '';
            setEmail(target.value);
          }}
        />
        {validation.email && <span>{validation.email}</span>}
        <input
          type="text"
          name="cpf"
          placeholder="CPF"
          value={cpf}
          onChange={({ target }) => {
            validation.cpf = '';
            setCpf(target.value);
          }}
        />
        {validation.cpf && <span>{validation.cpf}</span>}
        <input
          type="text"
          name="phone"
          placeholder="Telefone"
          value={phone}
          onChange={({ target }) => {
            validation.phone = '';
            setPhone(target.value);
          }}
        />
        {validation.phone && <span>{validation.phone}</span>}

        <footer>
          <Button type="button" onClick={onClose}>
            Voltar
          </Button>
          <Button type="submit" primary>
            Enviar
          </Button>
        </footer>
      </Form>
    </>
  );
}
