import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { FaEdit, FaDownload, FaRegTrashAlt } from 'react-icons/fa';
import { format } from 'date-fns';

import api from '../../../services/api';

import { Container, Title, Button } from './styles';

export default function ContractList() {
  const [contracts, setContracts] = useState([]);

  useEffect(() => {
    async function loadContracts() {
      const response = await api.get('/contracts');

      const data = response.data.map(item => {
        const parseStartDate = new Date(parseInt(item.startDate, 0));

        const parseEndDate = new Date(parseInt(item.endDate, 0));

        return {
          ...item,
          startDate: format(parseStartDate, 'MM/dd/yyyy'),
          endDate: format(parseEndDate, 'MM/dd/yyyy'),
        };
      });

      setContracts(data);
    }

    loadContracts();
  }, []);

  async function handleDelete(id) {
    await api.delete(`contracts/${id}`);

    const allContracts = [...contracts];

    const index = allContracts.findIndex(item => item.id === id);

    allContracts.splice(index, 1);

    setContracts(allContracts);
  }

  return (
    <>
      <Title>List Contract</Title>
      <Container>
        <ul>
          {contracts.map(item => (
            <li key={String(item.id)}>
              <h2>{item.title}</h2>
              <section>
                <div>
                  <strong>Início do contrato</strong>
                  <span>{item.startDate}</span>
                </div>
                <div>
                  <strong>Término do contrato</strong>
                  <span>{item.endDate}</span>
                </div>
              </section>
              <div>
                <Link to={`newcontract/${item.id}`}>
                  <FaEdit size={20} color="#000" />
                </Link>
                {item.file && (
                  <a
                    href={`http://localhost:3333/files/${item.file}`}
                    target="_blank"
                  >
                    <FaDownload size={20} color="#000" />
                  </a>
                )}
                <button type="button" onClick={() => handleDelete(item.id)}>
                  <FaRegTrashAlt size={20} color="#000" />
                </button>
              </div>
            </li>
          ))}
        </ul>
        <footer>
          <Button type="button" onClick={() => window.history.back()}>
            Voltar
          </Button>
        </footer>
      </Container>
    </>
  );
}
