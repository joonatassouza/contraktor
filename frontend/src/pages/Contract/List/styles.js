import styled from 'styled-components';

export const Container = styled.div`
  width: 900px;
  margin: auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  ul {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 500px;

    li {
      width: 100%;
      height: 150px;
      border-radius: 10px;
      background: #fff;

      & + li {
        margin-top: 10px;
      }

      h2 {
        margin: 10px 30px;
      }

      section {
        margin-top: 30px;
        display: flex;
        justify-content: space-around;

        strong {
          font-weight: normal;
          margin-right: 5px;
        }
      }

      > div {
        display: flex;
        margin-top: 20px;
        padding: 20px;
        justify-content: space-between;

        button {
          background: none;
          border: none;
        }
      }
    }
  }

  footer {
    margin-top: 50px;
    width: 500px;
  }
`;

export const Title = styled.h1`
  text-align: center;
  margin: 30px;
`;

export const Button = styled.button`
  width: 150px;
  height: 30px;
  border-radius: 5px;
  border: 2px solid #2d2d2d;
  background: transparent;
  font-size: 16px;
  font-weight: bold;
  color: #2d2d2d;
  text-transform: uppercase;
  letter-spacing: 2px;

  &:hover {
    background: #000;
    color: #fff;
    border-color: #000;
  }

  transition: 500ms all ease;
`;
