import React, { useState, useEffect } from 'react';
import DatePicker from 'react-datepicker';
import Dropzone from 'react-dropzone';
import Modal from 'react-modal';
import { FaEdit, FaTrashAlt } from 'react-icons/fa';
import * as Yup from 'yup';

import Part from '../../Part';

import api from '../../../services/api';

import { Form, Title, Droparea, Button } from './styles';

Modal.setAppElement('#root');

const schema = Yup.object().shape({
  title: Yup.string().required('Obrigatório'),
  startDate: Yup.date().required('Obrigatório'),
  endDate: Yup.date().required('Obrigatório'),
  file: Yup.mixed()
    .required('Arquivo obrigatório')
    .test(
      'fileFormat',
      'É permitido apenas .pdf e arquivos de texto .doc .docx .txt',
      value =>
        value &&
        [
          'edit',
          'application/pdf',
          'application/msword',
          'text/plain',
        ].includes(value.type)
    ),
});

export default function ContractNew({ match, history }) {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [partIdEdit, setPartIdEdit] = useState(null);

  const [contractId, setContractId] = useState(Number(match.params.id));
  const [title, setTitle] = useState('');
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [file, setFile] = useState(null);
  const [parts, setParts] = useState([]);
  const [validation, setValidation] = useState({});

  async function loadContract() {
    const response = await api.get(`contracts/${contractId}`);
    const parsedStartDate = new Date(parseInt(response.data.startDate, 0));
    const parsedEndDate = new Date(parseInt(response.data.endDate, 0));
    setTitle(response.data.title);
    setStartDate(parsedStartDate);
    setEndDate(parsedEndDate);
    setParts(response.data.parts);
    if (response.data.file) setFile({ name: response.data.file, type: 'edit' });
  }

  useEffect(() => {
    if (match.params.id > 0) {
      loadContract();
    }
  }, [contractId]);

  async function handleSubmit(e) {
    e.preventDefault();

    try {
      await schema.validate(
        { title, startDate, endDate, file },
        { abortEarly: false }
      );

      const data = new FormData();

      if (file && file.size) {
        data.append('file', file);
      }

      data.append('title', title);
      data.append('startDate', startDate.getTime());
      data.append('endDate', endDate.getTime());

      if (contractId) {
        await api.put(`contracts/${contractId}`, data);
      } else {
        const response = await api.post('contracts', data);

        setContractId(response.data.id);
      }
    } catch (err) {
      const validations = {};
      if (err.inner) {
        for (const error of err.inner) {
          validations[error.path] = error.message;
        }
      }

      setValidation(validations);
    }
  }

  async function handleDelete(id) {
    await api.delete(`parts/${id}`);

    const allParts = [...parts];

    const index = allParts.findIndex(item => item.id === id);

    allParts.splice(index, 1);

    setParts(allParts);
  }

  return (
    <>
      <Title>Novo Contrato</Title>
      <Form onSubmit={handleSubmit}>
        <input
          type="text"
          name="title"
          placeholder="Título"
          value={title}
          onChange={({ target }) => {
            validation.title = '';
            setTitle(target.value);
          }}
        />
        {validation.title && <span>{validation.title}</span>}
        <DatePicker
          selected={startDate}
          name="startDate"
          onChange={date => {
            validation.startDate = '';
            setStartDate(date);
          }}
          placeholderText="Data de inicio"
        />
        {validation.startDate && <span>{validation.startDate}</span>}
        <DatePicker
          selected={endDate}
          name="endDate"
          onChange={date => {
            validation.endDate = '';
            setEndDate(date);
          }}
          placeholderText="Data de vencimento"
        />
        {validation.endDate && <span>{validation.endDate}</span>}
        <Dropzone
          onDropAccepted={files => {
            validation.file = '';
            setFile(files[0]);
          }}
          value={file}
          accept="application/pdf,application/msword,text/plain"
        >
          {({ getRootProps, getInputProps }) => (
            <Droparea>
              <div {...getRootProps()}>
                <input {...getInputProps()} name="file" />
                <p>
                  {file
                    ? file.name
                    : 'Arraste e solte um arquivo aqui, ou clique para selecionar'}
                </p>
              </div>
            </Droparea>
          )}
        </Dropzone>
        {validation.file && <span>{validation.file}</span>}

        {!!contractId && (
          <article>
            <Button type="button" onClick={() => setModalIsOpen(true)}>
              Adicionar parte
            </Button>
            <div>
              <h2>Partes do contrato</h2>
              <ul>
                {parts.map(item => (
                  <li key={String(item.id)}>
                    <section>
                      <strong>
                        {item.firstname} {item.lastname}
                      </strong>
                      <p>{item.email}</p>
                    </section>
                    <div>
                      <button
                        type="button"
                        onClick={() => {
                          setPartIdEdit(item.id);
                          setModalIsOpen(true);
                        }}
                      >
                        <FaEdit size={15} />
                      </button>
                      <button
                        type="button"
                        onClick={() => handleDelete(item.id)}
                      >
                        <FaTrashAlt size={15} />
                      </button>
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </article>
        )}

        <footer>
          <Button type="button" onClick={() => window.history.back()}>
            Voltar
          </Button>
          <Button type="submit" primary>
            Enviar
          </Button>
        </footer>
      </Form>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={() => setModalIsOpen(false)}
        contentLabel="Example Modal"
        style={{
          content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            boxShaddow: '5px 5px 5px #000',
          },
        }}
      >
        <Part
          onClose={() => {
            setPartIdEdit(null);
            setModalIsOpen(false);
            loadContract();
          }}
          contractId={contractId}
          id={partIdEdit}
        />
      </Modal>
    </>
  );
}
