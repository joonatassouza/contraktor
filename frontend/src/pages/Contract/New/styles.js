import styled from 'styled-components';

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;

  input {
    width: 400px;
    height: 30px;
    background: rgba(0, 0, 0, 0.3);
    color: #fff;
    border: none;
    border-radius: 5px;
    margin: 5px;
    padding: 5px 10px;
  }

  span {
    color: red;
    font-weight: bold;
    font-size: 12px;
    margin-top: 0px;
    width: 400px;
  }

  article {
    margin-top: 30px;
    display: flex;
    width: 400px;
    flex-direction: column;
    align-items: flex-end;

    h2 {
      text-align: center;
      margin: 5px;
    }

    > div {
      margin-top: 30px;
      width: 100%;
      border: 1px solid #888;
      border-radius: 2px;

      li {
        display: flex;
        justify-content: space-between;
        align-items: center
        margin: 15px 15px;

        section {
          display: flex;
          flex-direction: column;

          p {
            color: #444
          }
        }

        button {
          border: none;
          background: none;
          margin-left: 5px;

          svg {
            transition: all 200ms ease;

            &:hover {
              width: 20px;
              height: 20px;
            }
          }
        }
      }
    }
  }

  footer {
    margin-top: 30px;
    display: flex;
    width: 400px;
    flex-direction: row;
    justify-content: space-between;
  }
`;

export const Button = styled.button`
  width: 150px;
  height: 40px;
  border-radius: 5px;
  border: 2px solid #2d2d2d;
  background: ${props => (props.primary ? '#2d2d2d' : 'transparent')};
  font-size: 14px;
  font-weight: bold;
  color: ${props => (props.primary ? 'white' : '#2d2d2d')};
  text-transform: uppercase;
  letter-spacing: 2px;

  &:hover {
    background: #000;
    color: #fff;
    border-color: #000;
  }

  transition: 500ms all ease;
`;

export const Title = styled.h2`
  margin: 30px;
  text-align: center;
`;

export const Droparea = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  border: 3px dashed #999;
  border-radius: 5px;
  width: 400px;
  height: 60px;
  margin-top: 10px;

  &:hover {
    background: #fafafa;
  }

  transition: 500ms background ease;

  p {
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 400px;
    height: 80px;
  }
`;
