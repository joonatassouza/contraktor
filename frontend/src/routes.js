import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Main from './pages/Main';
import ContractNew from './pages/Contract/New';
import ContractList from './pages/Contract/List';
import Part from './pages/Part';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Main} />
        <Route path="/contract" component={ContractList} />
        <Route path="/newcontract/:id" component={ContractNew} />
        <Route path="/part" component={Part} />
      </Switch>
    </BrowserRouter>
  );
}
